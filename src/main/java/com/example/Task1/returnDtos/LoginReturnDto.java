package com.example.Task1.returnDtos;

public class LoginReturnDto {
	private String firstName;
	private String lastName;
	private String organization;
	private String token;
	public LoginReturnDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginReturnDto(String firstName, String lastName, String organization, String token) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.organization = organization;
		this.token = token;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "LoginReturnDto [firstName=" + firstName + ", lastName=" + lastName + ", organization=" + organization
				+ ", token=" + token + "]";
	}
	
	
	
}
