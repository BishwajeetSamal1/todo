package com.example.Task1.returnDtos;

import java.util.List;

import com.example.Task1.model.list.ToDoList;

public class SearchTasksReturnDto {
	private List<ToDoList> tasks;
	private long totalSearchedRecordCount;
	public List<ToDoList> getTasks() {
		return tasks;
	}
	public void setTasks(List<ToDoList> tasks) {
		this.tasks = tasks;
	}
	public long getTotalRecordCount() {
		return totalSearchedRecordCount;
	}
	public void setTotalRecordCount(long totalRecordCount) {
		this.totalSearchedRecordCount = totalRecordCount;
	}
	@Override
	public String toString() {
		return "SearchTasksReturnDto [tasks=" + tasks + ", totalSearchedRecordCount=" + totalSearchedRecordCount + "]";
	}
	public SearchTasksReturnDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SearchTasksReturnDto(List<ToDoList> tasks, long totalSearchedRecordCount) {
		super();
		this.tasks = tasks;
		this.totalSearchedRecordCount = totalSearchedRecordCount;
	}
}
