package com.example.Task1.returnDtos;

import java.util.List;

import com.example.Task1.model.list.ToDoList;

public class TaskReturnDto {
	private List<ToDoList> tasks;
	private long totalRecordCount;
	public List<ToDoList> getTasks() {
		return tasks;
	}
	public void setTasks(List<ToDoList> tasks) {
		this.tasks = tasks;
	}
	public long getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	@Override
	public String toString() {
		return "TaskReturnDto [tasks=" + tasks + ", totalRecordCount=" + totalRecordCount + "]";
	}
	public TaskReturnDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TaskReturnDto(List<ToDoList> tasks, long totalRecordCount) {
		super();
		this.tasks = tasks;
		this.totalRecordCount = totalRecordCount;
	}
	
}
