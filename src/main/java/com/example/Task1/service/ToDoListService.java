package com.example.Task1.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.Task1.Dtos.AddTaskDto;
import com.example.Task1.Dtos.UpdateDto;
import com.example.Task1.http.response.RestResponse;
import com.example.Task1.model.list.ToDoList;


public interface ToDoListService {

	RestResponse addTask(AddTaskDto addTaskDto, long usersId );

	RestResponse getTasks(int pageSize, int pageNumber,long usersId);

	RestResponse updateTask(UpdateDto updateDto, long usersId);

	RestResponse setTrue(UpdateDto updateDto, long usersId);

	RestResponse searchTasks(String searchVal,int pageSize, int pageNumber, long usersId);
	
	
}
