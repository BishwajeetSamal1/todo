package com.example.Task1.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.example.Task1.Dtos.AddTaskDto;
import com.example.Task1.Dtos.UpdateDto;
import com.example.Task1.http.response.RestResponse;

@Component
public class SecondClass implements ToDoListService {
	public void show()
	{
		System.out.println("second class of todoservice");
	}

	@Override
	public RestResponse addTask(AddTaskDto addTaskDto, long usersId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RestResponse getTasks(int pageSize, int pageNumber, long usersId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RestResponse updateTask(UpdateDto updateDto, long usersId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RestResponse setTrue(UpdateDto updateDto, long usersId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RestResponse searchTasks(String searchVal, int pageSize, int pageNumber, long usersId) {
		// TODO Auto-generated method stub
		return null;
	}
}
