package com.example.Task1.service;

import com.example.Task1.Dtos.LoginDto;
import com.example.Task1.Dtos.RegisterDto;
import com.example.Task1.http.response.RestResponse;

public interface UsersServices {

	RestResponse registerUser(RegisterDto registerDto);

	RestResponse login(LoginDto loginDto);

	RestResponse logout(String token);
	
}
