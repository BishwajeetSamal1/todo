package com.example.Task1.service;
import java.sql.Timestamp;
import org.springframework.data.domain.PageRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.Task1.Dtos.AddTaskDto;
import com.example.Task1.Dtos.UpdateDto;
import com.example.Task1.dao.todolist.ToDoRepository;
import com.example.Task1.dao.users.UsersRepository;
import com.example.Task1.http.response.DataResponse;
import com.example.Task1.http.response.RestResponse;
import com.example.Task1.http.response.StatusCode;
import com.example.Task1.model.list.ToDoList;
import com.example.Task1.model.users.Users;
import com.example.Task1.returnDtos.SearchTasksReturnDto;
import com.example.Task1.returnDtos.TaskReturnDto;

//@Service
@Component
public class ToDoServiceImpl implements ToDoListService {
	@Autowired
	ToDoRepository toDoRepository;
	@Autowired
	UsersRepository usersRepository;
	@Override
	public RestResponse addTask(AddTaskDto addTaskDto, long usersId) {
		System.out.print(addTaskDto);
		Users u = usersRepository.findById(usersId);
		System.out.println("-----------------"+usersId+"----------------");
		LocalDateTime dateTime =addTaskDto.getDateTime();
		Timestamp timestamp_object = Timestamp.valueOf(dateTime);
        System.out.println("Time stamp : " + timestamp_object);
        System.out.println(timestamp_object.getTime());
        ToDoList list = new ToDoList();
		list.setActive(true);
		list.setTask(addTaskDto.getTask());
		list.setDescription(addTaskDto.getDescription());
		list.setDeadLine(timestamp_object.getTime());
		list.setUsers(u);
		toDoRepository.save(list);
		return new DataResponse(StatusCode.SUCCESS,"Task added",list);
	}
	@Override
	public RestResponse getTasks(int pageSize, int pageNumber,long usersId) {
		System.out.println("-------------"+pageSize+"+++++++++++++++"+pageNumber+"+++++++++"+usersId);
		Users u = usersRepository.findById(usersId);
		List<ToDoList> tasks = new ArrayList<ToDoList>();
		tasks = toDoRepository.findByUsersIdOrderByCreatedAtDesc(usersId,PageRequest.of((pageNumber-1), pageSize));	
		long totalRecordCount = toDoRepository.countByUsersId(usersId);
		TaskReturnDto taskReturnDto = new TaskReturnDto(tasks,totalRecordCount);
		System.out.println(taskReturnDto);
		return new DataResponse(StatusCode.SUCCESS,"Tasks returned",taskReturnDto);
	}
	@Override
	public RestResponse updateTask(UpdateDto updateDto,long usersId) {
		ToDoList tdl = toDoRepository.findById(updateDto.getId());
		if(!(tdl.getUsers().getId()==usersId))
			return new DataResponse(StatusCode.ERROR,"Task Mismatch",null);
		tdl.setActive(false);
		toDoRepository.save(tdl);
		return new DataResponse(StatusCode.SUCCESS,"Task updated",tdl);
	}
	@Override
	public RestResponse setTrue(UpdateDto updateDto,long usersId) {
		ToDoList tdl = toDoRepository.findById(updateDto.getId());
		if(!(tdl.getUsers().getId()==usersId))
			return new DataResponse(StatusCode.ERROR,"Task Mismatch",null);
		tdl.setActive(true);
		toDoRepository.save(tdl);
		return new DataResponse(StatusCode.SUCCESS,"Task updated",tdl);
	}
	@Override
	public RestResponse searchTasks(String searchVal,int pageSize, int pageNumber, long usersId) {
		System.out.println("impl111111111++++++++++++++++++++++++++++++++++++++++++++++++");
		List<ToDoList> searchedTasks = new ArrayList<ToDoList>();
		System.out.println("imp222222222222222222222222222++++++++++++++++++++++++++++++++++++++++++++++++");
		Users u = usersRepository.findById(usersId);
		searchedTasks = toDoRepository.findByTaskContainingIgnoreCase(searchVal,u,PageRequest.of((pageNumber-1), pageSize));
		long totalSearchedRecordCount = toDoRepository.countByTaskContainingIgnoreCase(searchVal,u);
		System.out.println("/777777777777777777777777777777777777777777777"+totalSearchedRecordCount);
		SearchTasksReturnDto searchTaskReturnDto = new SearchTasksReturnDto(searchedTasks,totalSearchedRecordCount);
		return new DataResponse(StatusCode.SUCCESS,"search results returned",searchTaskReturnDto);
	}
}
