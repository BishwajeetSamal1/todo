package com.example.Task1.service;

import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.Task1.Dtos.LoginDto;
import com.example.Task1.Dtos.RegisterDto;
import com.example.Task1.Validator.Validations;
import com.example.Task1.dao.token.TokenRepository;
import com.example.Task1.dao.users.UsersRepository;
import com.example.Task1.http.response.DataResponse;
import com.example.Task1.http.response.Messages;
import com.example.Task1.http.response.RestResponse;
import com.example.Task1.http.response.StatusCode;
import com.example.Task1.model.token.Token;
import com.example.Task1.model.users.Users;
import com.example.Task1.returnDtos.LoginReturnDto;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UsersServicesImpl implements UsersServices {
	
	@Autowired
	UsersRepository userRepository;
	
	@Autowired
	TokenRepository tokenRepository;
	
	@Autowired
	ModelMapper modelMapper;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Override
	public RestResponse registerUser(RegisterDto registerDto) {
		
		String[] requestedArray = {"userName" , "userPassword" , "firstName" , "lastName", "contactNo" , "emailId","gender","organization" };
		RestResponse rs = null;
		Validations validations = new Validations();
		
		
		rs = validations.validate(registerDto, requestedArray);

		if (rs!= null) {
			return rs;
		}
		
		
		Users u = null;
		u = userRepository.findByEmailIdIgnoreCase(registerDto.getEmailId());
		if(u!=null)
		{
			return new DataResponse(StatusCode.ALREADY_EXISTS, Messages.EMAIL_ALREADY_EXISTS, null);
		}
		u = userRepository.findByUserNameIgnoreCase(registerDto.getUserName());
		if(u!=null)
		{
			return new DataResponse(StatusCode.ALREADY_EXISTS, Messages.USER_NAME_ALREADY_EXISTS, null);
		}
		String password = bCryptPasswordEncoder.encode(registerDto.getUserPassword());
		u = modelMapper.map(registerDto, Users.class);
		u.setUserPassword(password);
		userRepository.save(u);
		return new DataResponse(StatusCode.SUCCESS, Messages.SIGN_UP_SUCESSFULLY_VERIFY_NOW, u);
	}


	@Override
	public RestResponse login(LoginDto loginDto) {
		String[] requestedArray = {"userName" , "password" };
		RestResponse rs = null;
		Validations validations = new Validations();
		System.out.println("111111111111111111");
		rs = validations.validate(loginDto, requestedArray);
		System.out.println("222222222222222222222222222");
		if (rs!= null) {
			System.out.println(rs);
			System.out.println("33333333333333333333333333333333333");
			return rs;
		}
		System.out.println("44444444444444444444444444444");
//		if(loginDto.getPassword()==""&&loginDto.getUserName()=="")
//			return new DataResponse(StatusCode.ID_CAN_NOT_NULL,"UserName and Password can not be null",null);
//		if(loginDto.getUserName()=="")
//			return new DataResponse(StatusCode.ID_CAN_NOT_NULL,"Username can not be null",null);		
//		if(loginDto.getPassword()=="")
//			return new DataResponse(StatusCode.ID_CAN_NOT_NULL,"Password can not be null",null);
		Users u = null;
		u = userRepository.findByUserNameIgnoreCase(loginDto.getUserName());
		if(u==null)
		{
			u = userRepository.findByEmailIdIgnoreCase(loginDto.getUserName());
			if(u==null)
				return new DataResponse(StatusCode.INVALID_CREDENTIALS_STATUS, Messages.USER_NOT_FOUND, null);
		}
		if(!(bCryptPasswordEncoder.matches(loginDto.getPassword(), u.getUserPassword())))
		{
			return new DataResponse(StatusCode.INVALID_CREDENTIALS_STATUS, Messages.INCORRECT_PASSWORD, null);
		}
		Token token = new Token();
		token.setUserId(u.getId());
		token.setUserToken(this.getToken(u.getId()));
		tokenRepository.save(token);
		LoginReturnDto loginReturnDto = new LoginReturnDto(u.getFirstName(),u.getLastName(),u.getOrganization(),token.getUserToken());
		System.out.println(loginReturnDto);
		return new DataResponse(StatusCode.SUCCESS, Messages.LOGIN_SUCESSFULLY, loginReturnDto);
	}


	private String getToken(long id) {
		String userId = ""+id;
		String token = Jwts.builder()
				.setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + 100_000_000))
				.signWith(SignatureAlgorithm.HS512, "MustBeUniqueEverwhere")
				.compact();
		return token;
	}


	@Override
	public RestResponse logout(String userToken) {
		Token token = tokenRepository.findByUserToken(userToken);
		System.out.println(token);
		tokenRepository.delete(token);
		return new DataResponse(StatusCode.SUCCESS,"token Deleted",null);
	}
	
}
