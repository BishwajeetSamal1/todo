package com.example.Task1.Dtos;

import java.time.LocalDateTime;

public class AddTaskDto {
	private String task;
	private String description;
	private LocalDateTime dateTime;
	private String token;
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "AddTaskDto [task=" + task + ", description=" + description + ", dateTime=" + dateTime + ", token="
				+ token + "]";
	}
	public AddTaskDto(String task, String description, LocalDateTime dateTime, String token) {
		super();
		this.task = task;
		this.description = description;
		this.dateTime = dateTime;
		this.token = token;
	}
	public AddTaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}

	

