package com.example.Task1.Dtos;

public class UpdateDto {
	long id;

	public UpdateDto(long id) {
		super();
		this.id = id;
	}

	public UpdateDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
