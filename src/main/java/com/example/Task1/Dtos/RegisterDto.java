package com.example.Task1.Dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.Task1.model.enums.ENUMS.Gender;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegisterDto {
	private long id;
	@NotNull(message = "USERNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "USERNAME SHOULD NOT BE EMPTY")
	private String userName;
	@NotNull(message = "USERPASSWORD SHOULD NOT BE NULL")
	@NotEmpty(message = "USERPASSWORD SHOULD NOT BE EMPTY")
	private String userPassword;
	@NotNull(message = "FIRSTNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "FIRST SHOULD NOT BE EMPTY")
	private String firstName;
	@NotNull(message = "LASTNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "LASTNAME SHOULD NOT BE EMPTY")
	private String lastName;
	@NotNull(message = "CONTACT SHOULD NOT BE NULL")
	@NotEmpty(message = "CONTACT SHOULD NOT BE EMPTY")
	private String contactNo;
	@NotNull(message = "Mail SHOLUD NOT BE NULL")
	@NotEmpty(message = "EMAIL SHOULD NOT BE EMPTY")
	@Email(message = "EMAIL SHOULD BE VALID")
	private String emailId;
	@NotNull(message = "GENDER SHOULD NOT BE NULL")
	
	private Gender gender;
	@NotNull(message = "ORGANIZATION SHOULD NOT BE NULL")
	@NotEmpty(message = "ORGANIZATION SHOULD NOT BE EMPTY")
	private String organization;
		
}
