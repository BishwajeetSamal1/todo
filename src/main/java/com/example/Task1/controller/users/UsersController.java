package com.example.Task1.controller.users;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Task1.Dtos.LoginDto;
import com.example.Task1.Dtos.RegisterDto;
import com.example.Task1.http.response.DataResponse;
import com.example.Task1.http.response.RestResponse;
import com.example.Task1.model.users.Users;
import com.example.Task1.service.UsersServices;

@RestController
@RequestMapping("/users")
public class UsersController {
	@Autowired
	UsersServices usersServices;

	@PostMapping("/registeruser")
	public RestResponse registerUser(@RequestBody RegisterDto registerDto) {
		try {
			return usersServices.registerUser(registerDto);			
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}

	@PostMapping("/login")
	public RestResponse login(@RequestBody LoginDto loginDto) {
		
		try {
			return usersServices.login(loginDto);	
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}

	@DeleteMapping("/logout")
	public RestResponse logout(HttpServletRequest req) {
		
		try {
			return usersServices.logout(req.getHeader("Authorization"));			
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}
}
