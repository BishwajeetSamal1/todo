package com.example.Task1.controller.todolist;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Task1.Dtos.AddTaskDto;
import com.example.Task1.Dtos.UpdateDto;
import com.example.Task1.http.response.DataResponse;
import com.example.Task1.http.response.RestResponse;
import com.example.Task1.model.list.ToDoList;
import com.example.Task1.service.ToDoListService;

@RestController
@RequestMapping("/tasks")
public class ToDoController {
	@Autowired
	@Qualifier("toDoServiceImpl")
	ToDoListService userService;
	
	
	@PostMapping("/addtask")
	public RestResponse addTask(@RequestBody AddTaskDto addTaskDto,HttpServletRequest req)
	{
		try
		{
			return userService.addTask(addTaskDto, Long.parseLong(req.getAttribute("id").toString()));           
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}

	@GetMapping("/displayall")
	public RestResponse displayAll(@RequestParam(required = false,defaultValue = "10")int pageSize,@RequestParam(required = false,defaultValue = "1")int pageNumber ,HttpServletRequest req)
	{
		try
		{
			return userService.getTasks(pageSize,pageNumber,Long.parseLong(req.getAttribute("id").toString()));
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}
	@PostMapping("/update")
	public RestResponse update(@RequestBody UpdateDto updateDto,HttpServletRequest req)
	{
		try
		{
			return userService.updateTask(updateDto,Long.parseLong(req.getAttribute("id").toString()));			
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}
	@PostMapping("/settrue")
	public RestResponse setTrue(@RequestBody UpdateDto updateDto,HttpServletRequest req)
	{
		try
		{
			return userService.setTrue(updateDto,Long.parseLong(req.getAttribute("id").toString()));
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
	}
	
	@GetMapping("/searchtasks")
	public RestResponse searchTasks(@RequestParam("searchVal")String searchVal,@RequestParam("pageSize")int pageSize,@RequestParam("pageNumber")int pageNumber ,HttpServletRequest req)
	{	
		try
		{
			return userService.searchTasks(searchVal,pageSize,pageNumber,Long.parseLong(req.getAttribute("id").toString()));
		}
		catch(Exception e)
		{
			return new DataResponse(500,e.getMessage(),null);
		}
		
	}
	
}
