package com.example.Task1.model.list;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.example.Task1.model.users.Users;

@Entity
public class ToDoList {
	@Id
	@GeneratedValue(generator = "list_generator")
	@SequenceGenerator(
			name = "list_generator",
			sequenceName = "list_sequence",
			initialValue = 1
			)
	private long id;
	@Column(columnDefinition="TEXT")
	private String task;
	private boolean isActive;
	@Column(columnDefinition="TEXT")
	private String description;
	private long createdAt = System.currentTimeMillis();
	private long updatedAt = System.currentTimeMillis();
	private long deadLine;
	private long deleteAt;
	@Column(nullable=false, columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean isDeleted=false;
	@ManyToOne
	Users users;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public long getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}
	public long getDeadLine() {
		return deadLine;
	}
	public void setDeadLine(long deadLine) {
		this.deadLine = deadLine;
	}
	public long getDeleteAt() {
		return deleteAt;
	}
	public void setDeleteAt(long deleteAt) {
		this.deleteAt = deleteAt;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public ToDoList() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	
	
}
