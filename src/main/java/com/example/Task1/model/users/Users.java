package com.example.Task1.model.users;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.example.Task1.model.commonfields.CommonFields;
import com.example.Task1.model.enums.ENUMS.Gender;
import com.example.Task1.model.list.ToDoList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Users extends CommonFields {
	@Id
	@GeneratedValue(generator = "user_generator")
	@SequenceGenerator(
			name = "user_generator",
			sequenceName = "user_sequence",
			initialValue = 100000
			)
	private long id;
	private String userName;
	private String userPassword;
	private String firstName;
	private String lastName;
	private String contactNo;
	private String emailId;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	private String organization;
	@OneToMany
	List<ToDoList> toDoList;	
}
