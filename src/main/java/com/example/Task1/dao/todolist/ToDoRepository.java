package com.example.Task1.dao.todolist;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.Task1.model.list.ToDoList;
import com.example.Task1.model.users.Users;

public interface ToDoRepository extends JpaRepository<ToDoList, Long> {
		List<ToDoList> findByUsersIdOrderByCreatedAtDesc(long usersId,Pageable pageable);
		ToDoList findById(long id);
		long countByUsersId(long usersId);
		
		
		
		@Query("Select t from ToDoList t where (t.users=?2) AND LOWER(t.task) LIKE LOWER(CONCAT('%',?1, '%')) OR (t.users=?2) AND LOWER(t.description) LIKE LOWER(CONCAT('%',?1, '%')) ORDER BY t.createdAt desc")
		List<ToDoList> findByTaskContainingIgnoreCase( String searchVal,Users users,Pageable pageable);

		@Query("Select count(t) from ToDoList t where (t.users=?2) AND LOWER(t.task) LIKE LOWER(CONCAT('%',?1, '%'))")
		long countByTaskContainingIgnoreCase( String searchVal,Users users);
	}

