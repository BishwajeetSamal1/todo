package com.example.Task1.dao.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Task1.model.users.Users;
@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

	Users findByUserNameIgnoreCase(String userName);

	Users findByEmailIdIgnoreCase(String emailId);
	
	Users findById(long id);

}
