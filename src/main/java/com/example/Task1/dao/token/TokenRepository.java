package com.example.Task1.dao.token;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Task1.model.token.Token;

public interface TokenRepository extends JpaRepository<Token, Long>{

	Token findByUserToken(String userToken);

	
}
